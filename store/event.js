
// const moment = require('moment')
// STATE AS VARIABLE
export const state = () => ({
  listImages: [],
  detailEvent: null,
})
// ACTIONS AS METHODS
export const actions = { // asyncronous

	// REGION ADMIN
  async getListImages ({ commit }, payload) {
    return await this.$axios.get(`web/event/list/gallery/fid-event=${payload}/page=1/limit=100/column-sort=id/sort=asc`)
      .then((response) => {
        // console.log(response.data)
        commit('setListImages', response.data.data)
		    // return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
      })
  },
  async getDetailEvent ({ commit }, payload) {
    return await this.$axios.get(`web/event/detail/url-name=${payload}`)
      .then((response) => {
        // console.log(response.data.data[0])
        commit('setDetailEvent', response.data.data[0])
        // return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
      })
  },
}
// MUTATION AS LOGIC
export const mutations = {
  setListImages(state, payload) {
    // console.log(payload)
    state.listImages = payload
  },
  setDetailEvent(state, payload) {
    // console.log(payload)
    state.detailEvent = payload
  },
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
  savedQuestion: state => {
    return state.savedQuestion
  },

}
