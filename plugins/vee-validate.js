import Vue from 'vue'
import { ValidationObserver, ValidationProvider, extend, localize } from 'vee-validate';
import * as rules from "vee-validate/dist/rules";
import id from "vee-validate/dist/locale/id.json";
import en from "vee-validate/dist/locale/en.json";

// extend('required', {
//   ...required,
//   // message: 'This field is required'
// });

// install rules and localization
Object.keys(rules).forEach(rule => {
  extend(rule, rules[rule]);
});

extend('password', {
  params: ['target'],
  validate(value, { target }) {
    return value === target;
  },
  message: 'Password confirmation does not match'
});

// localize('en', en);
localize('en', en);

extend('pos', value => {
  // return value >= 0;
  // console.log(value)
  return 'some error'

});

extend('terms', {
  validate (value) {
    return {
      required: true,
      valid: value
    };
  },
  computesRequired: true,
  message: 'You must agree to the Terms and Conditions.',
});

extend('pax', {
  validate (value) {
    return {
      required: true,
      valid: value > 0
    };
  },
  computesRequired: true,
  message: 'Please set up pax number',
});

// extend('terms', value => {
//   // return value >= 0;
//   console.log(value)
//   return value ? '' : 'You must agree to the Terms and Conditions.'
// });
// Register it globally
// main.js or any entry file.
Vue.component('ValidationProvider', ValidationProvider);
// Install components globally
Vue.component('ValidationObserver', ValidationObserver);
// Vue.config.productionTip = false;
