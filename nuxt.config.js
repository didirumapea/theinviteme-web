require('dotenv').config();

export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: "universal",
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: "server",
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    title: process.env.npm_package_name || "",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      { rel: "stylesheet", href: "/css/animate.css"},
      { rel: "stylesheet", href: "/css/icomoon.css"},
      { rel: "stylesheet", href: "/css/bootstrap.css"},
      { rel: "stylesheet", href: "/css/magnific-popup.css"},
      { rel: "stylesheet", href: "/css/owl.carousel.min.css"},
      { rel: "stylesheet", href: "/css/owl.theme.default.min.css"},
      { rel: "stylesheet", href: "/css/style.css"},

    ],
    router: {
      middleware: 'redirect',
    },
    script: [
      { type: 'text/javascript', src: '/js/modernizr-2.6.2.min.js' },
    //   {
    //     src: "/js/jquery.min.js",
    //     type: "text/javascript",
    //     body: true,
    //     defer: true
    //   },
    //   {
    //     src: "/js/jquery.easing.1.3.js",
    //     type: "text/javascript",
    //     body: true,
    //     defer: true
    //   },
    //   {
    //     src: "/js/bootstrap.min.js",
    //     type: "text/javascript",
    //     body: true,
    //     defer: true
    //   },
    //   {
    //     src: "/js/jquery.waypoints.min.js",
    //     type: "text/javascript",
    //     body: true,
    //     defer: true
    //   },
    //   {
    //     src: "/js/owl.carousel.min.js",
    //     type: "text/javascript",
    //     body: true,
    //     defer: true
    //   },
    //   {
    //     src: "/js/jquery.countTo.js",
    //     type: "text/javascript",
    //     body: true,
    //     defer: true
    //   },
    //   {
    //     src: "/js/jquery.stellar.min.js",
    //     type: "text/javascript",
    //     body: true,
    //     defer: true
    //   },
    //   {
    //     src: "/js/jquery.magnific-popup.min.js",
    //     type: "text/javascript",
    //     body: true,
    //     defer: true
    //   },
    //   {
    //     src: "/js/magnific-popup-options.js",
    //     type: "text/javascript",
    //     body: true,
    //     defer: true
    //   },
    //   {
    //     src: "/js/simplyCountdown.js",
    //     type: "text/javascript",
    //     body: true,
    //     defer: true
    //   },
    //   {
    //     src: "/js/main.js",
    //     type: "text/javascript",
    //     body: true,
    //     defer: true
      // }
    ]
  },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    '@/plugins/register-component',
    '@/plugins/axios.js',
    { src: '~plugins/vee-validate.js', ssr: false },
    { src: '~/plugins/vue-js-modal', ssr: false},
  ],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  server: {
    port: process.env.WEB_PORT, // default: 3000
    host: '0.0.0.0', // default: localhost,
    // port: 2604, // default: 3000
    // host: '192.168.1.188', // default: localhost,
    // timing: false,
    // https: {
    // 	key: fs.readFileSync(path.resolve(__dirname, '.https/server.key')),
    // 	cert: fs.readFileSync(path.resolve(__dirname, '.https/server.crt'))
    // }
  },
  axios: {
// ---------- maunual setting ---------
    // proxyHeaders: false,
    // credentials: false,
    baseURL: process.env.NODE_ENV !== "production"
      // ? `http://192.168.43.116:3132/api/v1/`
      ? `http://localhost:1819/api/v1/`
      // ? `https://api.bumikita.or.id/`
      : "https://api.theinviteme.com/",
    // ---------- proxy setting -----------
    // proxy: true, // Can be also an object with default options
    // prefix: '/api/'
  },
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@neneos/nuxt-animate.css',
    '@nuxtjs/axios',
  ],
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {},
};
