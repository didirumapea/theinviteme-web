import Vue from 'vue'
import VModal from 'vue-js-modal'
// import VModal from 'vue-js-modal/dist/ssr.index.js'

import 'vue-js-modal/dist/styles.css'
// import VModal from 'vue-js-modal/dist/ssr.index'

Vue.use(VModal, { dynamic: true, injectModalsContainer: true , dialog: true})
