let urlCdn = 'https://cdn.theinviteme.com/';
if (process.env.NODE_ENV === 'development'){
	// urlCdn = 'http://localhost:3132/theinviteme-files/';
	urlCdn = 'https://cdn.theinviteme.com/';
}

// let urlCdn = 'http://localhost:3132/';
// import moment from "moment";

// STATE AS VARIABLE
export const state = () => ({
    pathImagesWedding: urlCdn + 'assets/images/',
    d2: null,
    // timezone: moment().utcOffset()/60,
})
// ACTIONS AS METHODS
export const actions = { // asyncronous
  async getFeatured1 ({ commit }) {
    return await this.$axios.$get('home/product/type=1')
      .then((response) => {
        return response
      })
      .catch(err => {
        // alert(JSON.stringify(err.response))

        if (err.response === undefined){
          return 'no connection'
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
        //   return err.response
      })
  },
}
// MUTATION AS LOGIC
export const mutations = { // syncronous
  d1(state, payload){
    // console.log(payload)
    state.isTabActive = payload
  },
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
  pathImgPromoMerchant: state => {
    return state.pathImgPromoMerchant
  },
  pathImgCategory: state => {
    return state.pathImgCategory
  },
  pathImgBanner: state => {
    return state.pathImgBanner
  },
  pathImgMerchant: state => {
    return state.pathImgMerchant
  },
  pathImgMemberImages: state => {
    return state.pathImgMemberImages
  },
}
