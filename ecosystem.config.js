module.exports = {
  apps : [
    {
      name: "theinviteme-web-dev",
      script: "npm",
      args: "run dev"
    },
    {
      name: "theinviteme-web-prod",
      script: "npm",
      args: "run start"
    },
    {
      name: "theinviteme-web-staging",
      script: "npm",
      args: "run start"
    }
  ]
}
