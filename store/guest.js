
// const moment = require('moment')
// STATE AS VARIABLE
export const state = () => ({
  guestDetails: {
    guest_max_pax: 1,
    guest_name: 'John Appleseed',
    guest_phone: '',
    rsvp: null,
  }
	// user: null,
	// isLoggedIn: false,
	// count: 0,
	// userToken: null
})
// ACTIONS AS METHODS
export const actions = { // asyncronous

	// REGION ADMIN
  async getGuest ({ commit }, payload) {
    return await this.$axios.get(`web/guest/list/barcode=${payload}/page=1/limit=1/column-sort=id/sort=asc`)
      .then((response) => {
        // console.log(response.data)
        commit('setGuestDetail', response.data.data[0])
		    // return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
      })
  },
  async updateReservation ({ commit }, payload) {
    console.log(payload)
    return await this.$axios.post(`web/guest/update-reservation`, payload)
      .then((response) => {
        // console.log(response.data.message)
        // commit('setGuestDetail', response.data.data[0])
        return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
      })
  },
}
// MUTATION AS LOGIC
export const mutations = {
  setGuestDetail(state, payload) {
    state.guestDetails = payload
  },
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
  savedQuestion: state => {
    return state.savedQuestion
  },

}
