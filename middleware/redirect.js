export default async function ({route, redirect, store}) {
  // console.log()
  if (route.params.wedding !== 'andrinovel') {
    redirect('/')
    // redirect('/dashboard/v1')
  }
  if (store.state.event.detailEvent === null && Object.keys(route.params).length) {
    await store.dispatch('event/getDetailEvent', route.params.wedding)
  }

  // if (route.path === '/pages/issues/details' || route.path === '/pages/issues/details/') {
  // 	redirect('/pages/issues/details/kLRh2kWI')
  // }
}
